#include <stdlib.h>
#include <stdbool.h>

#include "RyanCIG.h"
#include "RyanLogger.h"

#include <ft2build.h>
#include FT_FREETYPE_H


struct rcigList
{
   unsigned char * base;
   size_t elementSize;
   size_t count;
   size_t size;
   size_t growBy;
};

static
void rciglInit(struct rcigList * list, size_t elementSize, 
                                       size_t growBy, size_t initSize)
{
   list->base = NULL;
   list->elementSize = elementSize;
   list->count = 0;
   if(growBy == 0)
   {
      list->growBy = 32;
   }
   else
   {
      list->growBy = growBy;
   }
   list->size = initSize;

   if(initSize > 0)
   {
      list->base = malloc(initSize * list->elementSize);
   }
}

static 
void rciglFree(struct rcigList * list)
{
   if(list->base != NULL)
   {
      free(list->base);
      list->base = NULL;
   }
   list->elementSize = 0;
   list->count       = 0;
   list->size        = 0;
   list->growBy      = 0;
}

static inline
void rciglMakeRoom(struct rcigList * list, size_t space)
{
   if(space >= list->size)
   {
      list->size = space + list->growBy;
      list->base = realloc(list->base, list->size * list->elementSize);
   }
}

static inline
unsigned char * rciglGetIndex(struct rcigList * list, size_t index)
{
   return list->base + (list->elementSize * index);
}

static
void * rciglAdd(struct rcigList * list, size_t * index)
{
   size_t i;
   rciglMakeRoom(list, list->count + 1);
   i = list->count;
   list->count ++;
   if(index != NULL)
   {
      (*index) = i;
   }
   return rciglGetIndex(list, i);
}

static
void rciglAddCopy(struct rcigList * list, const void * data, size_t * index)
{
   void * dest = rciglAdd(list, index);
   memcpy(dest, data, list->elementSize);
}

static inline
void rciglNoCheckSwap(struct rcigList * list, size_t index1, size_t index2)
{
   size_t tempIndex = list->count;
   unsigned char * ptr1, * ptr2, * ptrTemp;
   rciglMakeRoom(list, list->count + 1);
   ptr1    = rciglGetIndex(list, index1);
   ptr2    = rciglGetIndex(list, index2);
   ptrTemp = rciglGetIndex(list, tempIndex);
   
   memcpy(ptrTemp, ptr1,    list->elementSize);
   memcpy(ptr1,    ptr2,    list->elementSize);
   memcpy(ptr2,    ptrTemp, list->elementSize);
}

static
void * rciglRemoveFast(struct rcigList * list, size_t index)
{
   if(index < list->count)
   {
      if(index < list->count - 1)
      {
         rciglNoCheckSwap(list, index, list->count - 1);
      }
      list->count --;
      return rciglGetIndex(list, list->count);
   }
   return NULL;
}

static inline
void rciglSwap(struct rcigList * list, size_t index1, size_t index2)
{
   if(index1 < list->count &&
      index2 < list->count &&
      index1 != index2)
   {
      rciglNoCheckSwap(list, index1, index2);
   }
}

static
void rciglClear(struct rcigList * list)
{
   list->count = 0;
}

static
void * rciglGet(struct rcigList * list, size_t * count)
{
   if(count != NULL)
   {
      (*count) = list->count;
   }
   return list->base;
}

#define RCIGL_CREATE_TYPE(type, name)                                                           \
struct rcigList_ ## name                                                                        \
{                                                                                               \
   struct rcigList list;                                                                        \
};                                                                                              \
static inline                                                                                   \
void rciglInit_ ## name (struct rcigList_ ## name * list, size_t growBy, size_t initSize)       \
{                                                                                               \
   rciglInit(&list->list, sizeof(type), growBy, initSize);                                      \
}                                                                                               \
static inline                                                                                   \
void rciglFree_ ## name (struct rcigList_ ## name * list)                                       \
{                                                                                               \
   rciglFree(&list->list);                                                                      \
}                                                                                               \
static inline                                                                                   \
type * rciglAdd_ ## name (struct rcigList_ ## name * list, size_t * index)                      \
{                                                                                               \
   return rciglAdd(&list->list, index);                                                         \
}                                                                                               \
static inline                                                                                   \
void rciglAddCopy_ ## name (struct rcigList_ ## name * list, const type * data, size_t * index) \
{                                                                                               \
   rciglAddCopy(&list->list, data, index);                                                      \
}                                                                                               \
static inline                                                                                   \
type * rciglRemoveFast_ ## name (struct rcigList_ ## name * list, size_t index)                 \
{                                                                                               \
   return rciglRemoveFast(&list->list, index);                                                  \
}                                                                                               \
static inline                                                                                   \
type * rciglPop_ ## name (struct rcigList_ ## name * list)                                      \
{                                                                                               \
   return rciglRemoveFast(&list->list, list->list.count - 1);                                   \
}                                                                                               \
static inline                                                                                   \
void rciglClear_ ## name (struct rcigList_ ## name * list)                                      \
{                                                                                               \
   rciglClear(&list->list);                                                                     \
}                                                                                               \
static inline                                                                                   \
size_t rciglGetCount_ ## name (const struct rcigList_ ## name * list)                           \
{                                                                                               \
   return list->list.count;                                                                     \
}                                                                                               \
static inline                                                                                   \
type * rciglGet_ ## name (struct rcigList_ ## name * list, size_t * count)                      \
{                                                                                               \
   return rciglGet(&list->list, count);                                                         \
}                                                                                               \
static inline                                                                                   \
void rciglSwap_ ## name (struct rcigList_ ## name * list, size_t index1, size_t index2)         \
{                                                                                               \
   rciglSwap(&list->list, index1, index2);                                                      \
}


#define CONVERT(dt, name, src) dt name = (dt)src

struct rcigFont
{
   FT_Face ftFace;
};

RCIGL_CREATE_TYPE(struct rcigFont,   font)
RCIGL_CREATE_TYPE(struct rcigFont *, fontPtr)

struct rcigComponentPair
{
   const struct rcigComponent * component;
   void * componentData;
};

RCIGL_CREATE_TYPE(struct rcigComponentPair, compPair)

RCIGL_CREATE_TYPE(struct rcigDrawElement, de)

struct ricgPrivate
{
   const struct rcigBackend * backend;
   int windowWidth;
   int windowHeight;
   FT_Library ftLibrary;
   float vert_resolution;
   float hori_resolution;
   
   struct rcigFont * defaultFont;
   struct rcigList_font fontList;
   struct rcigList_fontPtr fontStack;
   
   void * fontTexture;
   
   void * testTexture;
   unsigned char fontOversampling;
   
   struct rcigList_compPair compPairList;
   size_t firstUnusedComponentIndex;
   bool newPassFlag;
   
   struct rcigList_de drawQueue;
};

static struct ricgPrivate Data;

static inline 
void * rcigCreateTextureFromU8RGBAData(void * imageData, 
                                       unsigned int width, unsigned int height,
                                       enum rcigImageFilterHint filterHint)
{
   return Data.backend->render.fpCreateTextureFromU8RGBAData(
              Data.backend->render.data, imageData, width, height, filterHint);
}

void rcigFreeTexture(void * texture)
{
   Data.backend->render.fpFreeTexture(Data.backend->render.data, texture);
}

struct ricgColor
{
   float red;
   float green;
   float blue;
   float alpha;
};

static inline
unsigned char rcigFloatColorElementToU8(float colorElement)
{
   if(colorElement > 1)
   {
      colorElement = 1;
   }
   else if(colorElement < 0)
   {
      colorElement = 0;
   }
   return 0xFF * colorElement;
}

static inline
void rcigConvertPalletDataToU8RGBA(void * u8rgba, 
                                   const struct ricgColor * pallet, 
                                   const unsigned char * palletImage, 
                                   unsigned int size)
{
   int i;
   unsigned char * outImage = (unsigned char *)u8rgba;
   for(i = 0; i < size; i++)
   {
      unsigned int palletImageIndex = i;
      unsigned int outImageIndex = i * 4;
      unsigned int palletIndex = palletImage[palletImageIndex];
      const struct ricgColor * color = &pallet[palletIndex];
      outImage[outImageIndex + 0] = rcigFloatColorElementToU8(color->red);
      outImage[outImageIndex + 1] = rcigFloatColorElementToU8(color->green);
      outImage[outImageIndex + 2] = rcigFloatColorElementToU8(color->blue);
      outImage[outImageIndex + 3] = rcigFloatColorElementToU8(color->alpha);
   }
}

static inline
void * rcigCreateTestTexture(void)
{
   #define TESTIMAGESIZE 8 * 8
   unsigned char u8rgba[TESTIMAGESIZE * 4];
   struct ricgColor pallet[] = {
      {0, 0, 0, 0},
      {1, 0, 0, 1},
      {0, 1, 0, 1},
      {0, 0, 1, 1},
   };
   unsigned char palletImage[TESTIMAGESIZE] = {
      0, 2, 2, 2, 2, 2, 2, 0,
      0, 2, 0, 0, 0, 0, 2, 0,
      0, 0, 1, 0, 0, 1, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0,
      0, 1, 0, 0, 0, 0, 1, 0,
      0, 0, 1, 1, 1, 1, 0, 0,
      0, 0, 0, 0, 0, 0, 3, 0,
      0, 0, 0, 3, 3, 3, 0, 3,
   };
   rcigConvertPalletDataToU8RGBA(u8rgba, pallet, palletImage, TESTIMAGESIZE);
   return rcigCreateTextureFromU8RGBAData(u8rgba, 8, 8, eRCIGIFH_Nearest);
}

void rcigSetup(const struct rcigBackend * backend, int windowWidth, int windowHeight)
{
   Data.backend = backend;
   Data.windowWidth = windowWidth;
   Data.windowHeight = windowHeight;
   Data.testTexture = rcigCreateTestTexture();
   FT_Init_FreeType(&Data.ftLibrary);
   Data.vert_resolution = 72;
   Data.hori_resolution = 72;
   
   rciglInit_font(&Data.fontList, 0, 0);
   rciglInit_fontPtr(&Data.fontStack, 0, 0);
   
   Data.fontTexture = NULL;
   Data.fontOversampling = 2;
   
   rciglInit_compPair(&Data.compPairList, 0, 0);
   Data.newPassFlag = true;
   
   rciglInit_de(&Data.drawQueue, 0, 0);
}

void rcigTeardown(void)
{
   size_t count, i;
   struct rcigFont * fonts;
   struct rcigComponentPair * pairs;

   // Free Components
   pairs = rciglGet_compPair(&Data.compPairList, &count);
   for(i = 0; i < count; i ++)
   {
      pairs[i].component->fpDestroy(pairs[i].componentData);
   }

   if(Data.backend->input.fpTeardown != NULL)
   {
      Data.backend->input.fpTeardown(Data.backend->input.data);
   }
   if(Data.backend->render.fpTeardown != NULL)
   {
      Data.backend->render.fpTeardown(Data.backend->render.data);
   }
   rcigFreeTexture(Data.testTexture);


   // Free Fonts
   fonts = rciglGet_font(&Data.fontList, &count);
   for(i = 0; i < count; i++)
   {
      FT_Done_Face(fonts[i].ftFace);
   }
   
   

   rciglFree_font(&Data.fontList);
   rciglFree_fontPtr(&Data.fontStack);
   rciglFree_compPair(&Data.compPairList);
   rciglFree_de(&Data.drawQueue);
   Data.defaultFont = NULL;
   if(Data.fontTexture != NULL)
   {
      rcigFreeTexture(Data.fontTexture);
      Data.fontTexture = NULL;
   }


   FT_Done_FreeType( Data.ftLibrary );

   Data.backend = NULL;
}

static inline
void rcigRenderTest(void)
{
   void * renderData = Data.backend->render.data;
   struct rcigDrawElement de;
   unsigned short indexData[6] = {0, 3, 1, 1, 3, 2};
   struct rcigGeometryData geometryData[4] = {
      { 100, 100, 0, 1, 1, 0, 0, 1 },
      { 100, 300, 0, 0, 0, 1, 0, 1 },
      { 300, 300, 1, 0, 0, 0, 1, 1 },
      { 300, 100, 1, 1, 1, 1, 1, 1 },
   };
   
   de.type              = eRCIGDET_Triangles;
   de.indexDataCount    = 6;
   de.indexData         = indexData;
   de.geometryDataCount = 4;
   de.geometryData      = geometryData;
   if(Data.fontTexture != NULL)
   {
      de.texture = Data.fontTexture;
   }
   else
   {
      de.texture = Data.testTexture;
   }

   Data.backend->render.fpDraw(renderData, 1, &de, 
                               Data.windowWidth, Data.windowHeight);
   
}

void rcigRender(void)
{
   void * renderData = Data.backend->render.data;
   
   struct rcigDrawElement * deList;
   struct rcigComponentPair * pairs;
   size_t i, count;
   int x, y, width, height;
   
   rciglClear_de(&Data.drawQueue);
   
   pairs = rciglGet_compPair(&Data.compPairList, &count);
   
   x = 0;
   y = 0;
   for(i = 0; i < Data.firstUnusedComponentIndex; i++)
   {
      pairs[i].component->fpRender(pairs[i].componentData, x, y, &width, &height);
      y += height;
      
   }
   
   deList = rciglGet_de(&Data.drawQueue, &count);
   Data.backend->render.fpDraw(renderData, count, deList, 
                               Data.windowWidth, Data.windowHeight);
   
   
   Data.newPassFlag = true;
}

void rcigSetScreenDPI(float horiDPI, float vertDPI)
{
   Data.hori_resolution = horiDPI;
   Data.vert_resolution = vertDPI;
}


struct rcigFont * rcigLoadFont(const char * filename, float pointSize)
{
   struct rcigFont * font = rciglAdd_font(&Data.fontList, NULL);
   
   FT_New_Face( Data.ftLibrary, filename, 0, &font->ftFace );
   FT_Set_Char_Size(font->ftFace, pointSize * 64 * Data.fontOversampling, 0, 
                    Data.hori_resolution, Data.vert_resolution);
   if(Data.defaultFont == NULL)
   {
      Data.defaultFont = font;
   }
   return font;
}

static inline
void rcigBlitFreeTypeBitmapFillData(unsigned char * startU8RGBA, 
                                    FT_Pixel_Mode pixel_mode, 
                                    const unsigned char * fpRow, int fpX)
{
   int byteIndex;
   int bitIndex;
   unsigned char shiftValue;
   unsigned char value;
   switch(pixel_mode)
   {
   case FT_PIXEL_MODE_MONO:
      byteIndex  = fpX / 8;
      bitIndex   = fpX % 8;
      shiftValue = fpRow[byteIndex] >> (7 - bitIndex);
      value      = (shiftValue & 0x01) * 0xFF;
      
      startU8RGBA[0] = 0xFF;
      startU8RGBA[1] = 0xFF;
      startU8RGBA[2] = 0xFF;
      startU8RGBA[3] = value;
      break;
   case FT_PIXEL_MODE_GRAY:
      byteIndex = fpX;
      startU8RGBA[0] = 0xFF;
      startU8RGBA[1] = 0xFF;
      startU8RGBA[2] = 0xFF;
      startU8RGBA[3] = fpRow[byteIndex];
      break;
   case FT_PIXEL_MODE_GRAY2:
      byteIndex  = fpX / 4;
      bitIndex   = (fpX % 4) * 2;
      shiftValue = fpRow[byteIndex] >> (7 - bitIndex);
      value      = ((shiftValue & 0x03) * (unsigned int)0xFF) / 0x03;
      
      startU8RGBA[0] = 0xFF;
      startU8RGBA[1] = 0xFF;
      startU8RGBA[2] = 0xFF;
      startU8RGBA[3] = value;
      break;
   case FT_PIXEL_MODE_GRAY4:
      byteIndex  = fpX / 2;
      bitIndex   = (fpX % 2) * 4;
      shiftValue = fpRow[byteIndex] >> (7 - bitIndex);
      value      = ((shiftValue & 0x0F) * (unsigned int)0xFF) / 0xF;
      
      startU8RGBA[0] = 0xFF;
      startU8RGBA[1] = 0xFF;
      startU8RGBA[2] = 0xFF;
      startU8RGBA[3] = value;
      break;
   case FT_PIXEL_MODE_BGRA:
      byteIndex = fpX * 4;
      startU8RGBA[0] = fpRow[byteIndex + 2];
      startU8RGBA[1] = fpRow[byteIndex + 1];
      startU8RGBA[2] = fpRow[byteIndex + 0];
      startU8RGBA[3] = fpRow[byteIndex + 3];
      break;
   }
}

static inline 
void rcigBlitFreeTypeBitmapOntoU8RGBA(void * u8rgba, int u8rgbaWidth, const FT_Bitmap * fpBitmap, 
                                      int x, int y)
{
   if(fpBitmap->pixel_mode == FT_PIXEL_MODE_MONO  ||
      fpBitmap->pixel_mode == FT_PIXEL_MODE_GRAY  ||
      fpBitmap->pixel_mode == FT_PIXEL_MODE_GRAY2 ||
      fpBitmap->pixel_mode == FT_PIXEL_MODE_GRAY4 ||
      fpBitmap->pixel_mode == FT_PIXEL_MODE_BGRA)
   {
      unsigned char * outData = (unsigned char*)u8rgba;
      const unsigned char * fpRow = fpBitmap->buffer;
      int sx, sy;
      const int maxX = x + fpBitmap->width;
      const int maxY = y + fpBitmap->rows;
      
      for(sy = 0; sy < fpBitmap->rows; sy ++)
      {
         for(sx = 0; sx < fpBitmap->width; sx++)
         {
            const unsigned int outIndex = ((sx + x) + ((sy + y) * u8rgbaWidth)) * 4;
            
            rcigBlitFreeTypeBitmapFillData(&outData[outIndex],
                                           fpBitmap->pixel_mode, fpRow, sx);
         }
         fpRow += fpBitmap->pitch;
      }
   }
   else
   {
      rlmLogError("Unsupported Free Type pixel Mode: %d\n", fpBitmap->pixel_mode);
   }
   
}

void rcidCreateFontTexture(void)
{
   unsigned char u8rgba[200 * 200 * 4];
   FT_ULong  charcode;
   FT_UInt   gindex;
   int nextX, nextY;
   int maxY;
   int textureWidth  = 200;
   int textureHeight = 200;
   memset(u8rgba, 0, 200 * 200 * 4);
   if(Data.fontTexture != NULL)
   {
      rcigFreeTexture(Data.fontTexture);
   }
   
   charcode = FT_Get_First_Char(Data.defaultFont->ftFace, &gindex);
   nextX = 0;
   nextY = 0;
   maxY = 0;
   while(gindex != 0 )
   {
      FT_Bitmap * bitmap;
      FT_Load_Char( Data.defaultFont->ftFace, charcode, FT_LOAD_RENDER ); 
      bitmap = &Data.defaultFont->ftFace->glyph->bitmap;
      if(nextX + bitmap->width >= textureWidth)
      {
         nextX = 0;
         nextY += maxY;
         maxY = 0;
      }
      if(nextY + bitmap->rows >= textureHeight)
      {
         break;
      }
      rcigBlitFreeTypeBitmapOntoU8RGBA(u8rgba, 200, bitmap, nextX, nextY);
      
      if(maxY < bitmap->rows)
      {
         maxY = bitmap->rows;
      }
      nextX += bitmap->width;
      
      charcode = FT_Get_Next_Char( Data.defaultFont->ftFace, charcode, &gindex );
   }
   
   
   
   Data.fontTexture = rcigCreateTextureFromU8RGBAData(u8rgba, 200, 200, eRCIGIFH_Linear);
}

void rcigFontPush(const struct rcigFont * font)
{
   rciglAddCopy_fontPtr(&Data.fontStack, &font, NULL);
}

struct rcigFont * rcigFontPop(void)
{
   struct rcigFont ** fontPtr = rciglPop_fontPtr(&Data.fontStack);
   return *fontPtr;
}



void rcigPanelBegin(void)
{
}

void rcigPanelEnd(void)
{
}

static inline 
void rcigHandelNewPass(void)
{
   if(Data.newPassFlag)
   {
      Data.newPassFlag = false;
      Data.firstUnusedComponentIndex = 0;
   }
}

static inline
void * rcigFindNextUnusedMatchingComponentData(const struct rcigComponent * component, 
                                               const void * componentSpecData,
                                               size_t * index)
{
   size_t i, count;
   struct rcigComponentPair * pairs;
   pairs = rciglGet_compPair(&Data.compPairList, &count);
   for(i = Data.firstUnusedComponentIndex; i < count; i++)
   {
      if(component == pairs[i].component &&
         component->fpCompair(pairs[i].componentData, componentSpecData) == 0)
      {
         if(index != NULL)
         {
            (*index) = i;
         }
         return pairs[i].componentData;
      }
   }
   return NULL;
}

void * rcigComponent(const struct rcigComponent * component, const void * componentSpecData)
{
   rcigHandelNewPass();
   {
      size_t index;
      void * componentData = rcigFindNextUnusedMatchingComponentData(component, componentSpecData, &index);
      
      if(componentData == NULL)
      {
         struct rcigComponentPair * pair;
         
         componentData = component->fpCreate(componentSpecData);
         
         pair = rciglAdd_compPair(&Data.compPairList, &index);
         pair->component = component;         
         pair->componentData = componentData;
      }
      rciglSwap_compPair(&Data.compPairList, Data.firstUnusedComponentIndex, index);
      Data.firstUnusedComponentIndex ++;
      return componentData;
   }
   
}

void rcigQueueDrawElement(const struct rcigDrawElement * element)
{
   (void)rciglAddCopy_de(&Data.drawQueue, element, NULL);
}

static const unsigned short rcigQuadIndexDataCCW[6] = {
   1, 0, 3, 1, 3, 2
};

void rcigDrawElementSetTexturedQuad(struct rcigDrawElement * element,
                                    struct rcigGeometryData * geometryData,
                                    void * texture, int x, int y, 
                                    int width, int height)
{
   rcigGeometryDataSet(&geometryData[0], x,         y,          0, 1, 1, 1, 1, 1);
   rcigGeometryDataSet(&geometryData[1], x + width, y,          1, 1, 1, 1, 1, 1);
   rcigGeometryDataSet(&geometryData[2], x + width, y + height, 1, 0, 1, 1, 1, 1);
   rcigGeometryDataSet(&geometryData[3], x,         y + height, 0, 0, 1, 1, 1, 1);
   
   element->type              = eRCIGDET_Triangles;
   element->indexDataCount    = 6;
   element->indexData         = rcigQuadIndexDataCCW;
   element->geometryDataCount = 4;
   element->geometryData      = geometryData;
   element->texture           = texture;
   
}

void * rcigCreateTextTexture(const char * text, int * textureWidth, int * textureHeight)
{
   FT_Face face = Data.defaultFont->ftFace;
   FT_GlyphSlot  slot = face->glyph;
   int width  = 0;
   int height = 0;
   int pen_x, pen_y;
   const char * c;
   FT_UInt glyph_index, prev_glyph_index;
   int assender = face->size->metrics.ascender >> 6;
   FT_Bool use_kerning;
   
   unsigned char * u8rgba;
   void * texture;
   
   use_kerning = FT_HAS_KERNING(face);
   
   if(text[0] != '\0')
   {
      height = (face->size->metrics.height >> 6) + 1;
   }
   
   c = text;
   pen_x = pen_y = 0;
   prev_glyph_index = 0;
   while((*c) != '\0')
   {
      glyph_index = FT_Get_Char_Index(face, *c);
      if(FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT))
      {
         // Ignore Errors for now
         continue;
      }
      
      if(use_kerning && glyph_index && prev_glyph_index)
      {
         FT_Vector delta;
         FT_Get_Kerning(face, prev_glyph_index, glyph_index, FT_KERNING_DEFAULT, &delta);
         pen_x += delta.x >> 6;
      }
      
      pen_x += slot->advance.x >> 6;
      pen_y += slot->advance.y >> 6;
      prev_glyph_index = glyph_index;
      c++;
   }
   
   width = pen_x;
   
   rlmLogDebug("Text Bitmap Image \"%s\" is (%d, %d)", text, width, height);
   
   u8rgba = malloc(4 * width * height);
   memset(u8rgba, 0, 4 * width * height);
   
   c = text;
   pen_x = 0;
   pen_y = 0;
   prev_glyph_index = 0;
   while((*c) != '\0')
   {
      glyph_index = FT_Get_Char_Index(face, *c);
      if(FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER))
      {
         // Ignore Errors for now
         rlmLogDebug("FT_Load_Char Error");
         continue;
      }
      
      if(use_kerning && glyph_index && prev_glyph_index)
      {
         FT_Vector delta;
         FT_Get_Kerning(face, prev_glyph_index, glyph_index, FT_KERNING_DEFAULT, &delta);
         pen_x += delta.x >> 6;
      }
      
      
      rcigBlitFreeTypeBitmapOntoU8RGBA(u8rgba, width, &slot->bitmap, pen_x + slot->bitmap_left, pen_y + assender - slot->bitmap_top);
      
      pen_x += slot->advance.x >> 6;
      pen_y += slot->advance.y >> 6;
      prev_glyph_index = glyph_index;
      c++;
   }
   texture = rcigCreateTextureFromU8RGBAData(u8rgba, width, height, eRCIGIFH_Linear);
   
   free(u8rgba);
   
   if(textureWidth != NULL)
   {
      (*textureWidth) = width / Data.fontOversampling;
   }
   if(textureHeight != NULL)
   {
      (*textureHeight) = height / Data.fontOversampling;
   }
   
   return texture;
   
}

// Label Data
struct rcigLabelData
{
   char * text;
   struct rcigGeometryData quadPoints[4];
   void * textTexture;
   int textureWidth;
   int textureHeight;
};

static 
void * ricgCompLabelCreate(const void * componentSpecData)
{
   struct rcigLabelData * data = malloc(sizeof(struct rcigLabelData));
   const struct rcigLabelData * spec = componentSpecData;
   data->text = strdup(spec->text);
   data->textTexture = rcigCreateTextTexture(data->text, &data->textureWidth, &data->textureHeight);
   return data;
}
static 
void ricgCompLabelDestroy(void * componentData)
{
   struct rcigLabelData * data = componentData;
   free(data->text);
   rcigFreeTexture(data->textTexture);
   free(data);
}

static 
int ricgCompLabelCompair(const void * componentData, const void * componentSpecData)
{
   const struct rcigLabelData * labelData  = componentData;
   const struct rcigLabelData * labelSpecData = componentSpecData;
   return strcmp(labelData->text, labelSpecData->text);
}

static
void ricgCompLabelRender(void * componentData, int x, int y, int * width, int * height)
{
   struct rcigLabelData * data = componentData;
   struct rcigDrawElement ele;
   rcigDrawElementSetTexturedQuad(&ele, data->quadPoints, data->textTexture, x, y, data->textureWidth, data->textureHeight);
   rcigQueueDrawElement(&ele);
   
   (*width)  = data->textureWidth;
   (*height) = data->textureHeight;
}


const struct rcigComponent rcigCompLabel = {
   ricgCompLabelCreate,
   ricgCompLabelDestroy,
   ricgCompLabelCompair,
   ricgCompLabelRender,
};



void rcigLabel(const char * text)
{
   struct rcigLabelData labelSpecData;
   labelSpecData.text = (char *)text;
   (void)rcigComponent(&rcigCompLabel, &labelSpecData);
}

