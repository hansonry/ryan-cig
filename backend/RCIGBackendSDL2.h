#ifndef __RCIGBACKENDSDL2_H__
#define __RCIGBACKENDSDL2_H__

#include <SDL.h>

#include <RyanCIG.h>


void rcigbsdl2SetupInput(struct rcigInputBackend * input);

// Returns true if event was used.
int rcigbsdl2HandleEvent(struct rcigInputBackend * input, SDL_Event * event);

#endif //__RCIGBACKENDSDL2_H__
