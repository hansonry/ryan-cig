#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include "RCIGBackendOpenGL.h"
#include <RyanLogger.h>

struct ricgOpenGLRenderData 
{
   GLuint shader;
   GLint uniformMatrix;
   GLint uniformTexture;
   void * whiteTexture;
};

struct ricgOpenGLTextureData
{
   GLuint texture;
};

static const char * VertexShaderText = 
   "#version 330 core\n"
   "layout (location = 0) in vec2 aPos;\n"
   "layout (location = 1) in vec2 aUV;\n"
   "layout (location = 2) in vec4 color;\n"
   "out vec4 vColor;\n"
   "out vec2 vUV;\n"
   "uniform mat4 matrix;\n"
   "void main()\n"
   "{\n"
   "   gl_Position = matrix * vec4(aPos, 0.0, 1.0);\n"
   "   vColor = color;\n"
   "   vUV = aUV;\n"
   "}\n";


static const char * FragmentShaderText = 
   "#version 330 core\n"
   "in vec4 vColor;\n"
   "in vec2 vUV;\n"
   "out vec4 FragColor;\n"
   "uniform sampler2D theTexture;\n"
   "void main()\n"
   "{\n"
   "   FragColor = vColor * texture(theTexture, vUV);\n"
   "}\n";

static inline
void rcigbopenglImageVirtFlip(void * outImageData, const void * inImageData, 
                              unsigned int width, unsigned int height)
{
   const unsigned int rowLength = width * 4;
   unsigned char * outByteData = outImageData;
   const unsigned char * inByteData = inImageData;
   
   outByteData += rowLength * (height - 1);
   int y;
   for(y = 0; y < height; y++)
   {
      memcpy(outByteData, inByteData, rowLength);
      outByteData -= rowLength;
      inByteData += rowLength;
   }
   
}

static
void * rcigbopenglCreateTextureFromU8RGBAData(void * data, const void * imageData, 
                                              unsigned int width, 
                                              unsigned int height, 
                                              enum rcigImageFilterHint filterHint)
{
   struct ricgOpenGLTextureData * textureData = malloc(sizeof(struct ricgOpenGLTextureData));
   (void)data;
   void * flipedImageData = malloc(width * height * 4);
   rcigbopenglImageVirtFlip(flipedImageData, imageData, width, height);
   
   
   glGenTextures(1, &textureData->texture);
   glBindTexture(GL_TEXTURE_2D, textureData->texture);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
   
   switch(filterHint)
   {
   case eRCIGIFH_Nearest:
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      break;
   default:
   case eRCIGIFH_Linear:
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      break;
   }
   
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, 
                GL_UNSIGNED_BYTE, flipedImageData);
                
   glGenerateMipmap(GL_TEXTURE_2D);
   
   free(flipedImageData);
   
   return textureData;
}

static
void rcigbopenglFreeTexture(void * data, void * texture)
{
   struct ricgOpenGLTextureData * textureData = (struct ricgOpenGLTextureData *)texture;
   (void)data;
   glDeleteTextures(1, &textureData->texture);
   free(texture);
}

static inline
void SetupTopDownMatrix(GLfloat * matrix, int windowWidth, int windowHeight)
{
   matrix[0]  = 2 / (float)windowWidth;
   matrix[4]  = 0;
   matrix[8]  = 0;
   matrix[12] = -1;
   
   matrix[1]  = 0;
   matrix[5]  = -2 / (float)windowHeight;
   matrix[9]  = 0;
   matrix[13] = 1;
   
   matrix[2]  = 0;
   matrix[6]  = 0;
   matrix[10] = 1;
   matrix[14] = 0;
   
   matrix[3]  = 0;
   matrix[7]  = 0;
   matrix[11] = 0;
   matrix[15] = 1;

}

static
void rcigbopenglDraw(void * data, unsigned int drawElementCount, 
                     const struct rcigDrawElement * drawElementList,
                     int windowWidth, int windowHeight)
{
   unsigned int i;
   GLuint vbo, vao;
   GLfloat matrixData[16];
   struct ricgOpenGLRenderData * rend = (struct ricgOpenGLRenderData *) data;

   SetupTopDownMatrix(matrixData, windowWidth, windowHeight);
   
   glEnable(GL_BLEND); 
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   
   glGenVertexArrays(1, &vao);
   glBindVertexArray(vao);

   glUseProgram(rend->shader);
   glUniformMatrix4fv(rend->uniformMatrix, 1, GL_FALSE, matrixData);
   
   
   glActiveTexture(GL_TEXTURE0);
   glUniform1i(rend->uniformTexture, 0);
   
   for(i = 0; i < drawElementCount; i++)
   {
      const struct rcigDrawElement * de = &drawElementList[i];
      const struct ricgOpenGLTextureData * textureData = (const struct ricgOpenGLTextureData *)de->texture;
      GLuint vbo, indexBuffer;
      
      if(textureData == NULL)
      {
         textureData = rend->whiteTexture;
      }
      
      glGenBuffers(1, &vbo);
      glBindBuffer(GL_ARRAY_BUFFER, vbo);
      glBufferData(GL_ARRAY_BUFFER, de->geometryDataCount * sizeof(struct rcigGeometryData), de->geometryData, GL_STATIC_DRAW);
      
      glGenBuffers(1, &indexBuffer);      
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, de->indexDataCount * sizeof(short), de->indexData, GL_STATIC_DRAW);
      glEnableVertexAttribArray(0);
      glEnableVertexAttribArray(1);
      glEnableVertexAttribArray(2);
      glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(struct rcigGeometryData), (void*)0);   
      glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(struct rcigGeometryData), (void*)offsetof(struct rcigGeometryData, u));   
      glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(struct rcigGeometryData), (void*)offsetof(struct rcigGeometryData, colorR));   

      glBindTexture(GL_TEXTURE_2D, textureData->texture);

      switch(de->type)
      {
      case eRCIGDET_Points:
         glDrawElements(GL_POINTS,     de->indexDataCount, GL_UNSIGNED_SHORT, (void*)0);
         break;
      case eRCIGDET_Lines:
         glDrawElements(GL_LINE_STRIP, de->indexDataCount, GL_UNSIGNED_SHORT, (void*)0);
         break;
      case eRCIGDET_Triangles:
         glDrawElements(GL_TRIANGLES,  de->indexDataCount, GL_UNSIGNED_SHORT, (void*)0);
         break;
      default:
         rlmLogError("Unexpected Draw Element Type: %d\n", de->type);
         break;
      }
      
      glDeleteBuffers(1, &vbo);
      glDeleteBuffers(1, &indexBuffer);
   }
   glDeleteVertexArrays(1, &vao);
   
}


void rcigbopenglTeardown(void * data)
{
   struct ricgOpenGLRenderData * rend = (struct ricgOpenGLRenderData *) data;
   rcigbopenglFreeTexture(data, rend->whiteTexture);
   glDeleteProgram(rend->shader);
   free(rend);
}

static inline
void * rcigbopenglCreateWhiteTexture(struct ricgOpenGLRenderData * data)
{
   const unsigned char whiteData[4] = {255, 255, 255, 255};
   return rcigbopenglCreateTextureFromU8RGBAData(data, whiteData, 1, 1, 
                                                 eRCIGIFH_Nearest);
}

static inline
GLuint rcigbopenglCreateShaderProgram(void)
{
   GLint length;
   GLenum error;
   GLint success = 0;
   GLuint vertexShader, fragmentShader, program;
  
   // Create Vertex Shader
   vertexShader = glCreateShader(GL_VERTEX_SHADER);
   rlmAssert(vertexShader != 0, "Failed to Create Vertex Shader: 0x%04X", glGetError());
   rlmLogDebug("Vertex Shader Created");
   
   // Load Vertex Shader
   length = strlen(VertexShaderText);
   glShaderSource(vertexShader, 1, &VertexShaderText, &length);
   error = glGetError();
   rlmAssert(error == GL_NO_ERROR, "Error loading Vertex Shader Source: 0x%04X", error);
   rlmLogDebug("Vertex Shader Loaded");
   
   // Compile Vertex Shader
   glCompileShader(vertexShader);
   glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
   if(success == GL_FALSE)
   {
      GLint maxLength = 0;
      char * log;
      glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);
      log = malloc(maxLength);
      glGetShaderInfoLog(vertexShader, maxLength, &maxLength, log);
      rlmLogError("Vertex Shader Error Log:\n%s", log);
      free(log);
      glDeleteShader(vertexShader);
      rlmLogFatal("Vertex Shader Failed to Build");
   }
   rlmLogDebug("Vertex Shader Compiled");
   
   
   // Create Fragment Shader
   fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
   rlmAssert(fragmentShader != 0, "Failed to Create Fragment Shader: 0x%04X", glGetError());
   rlmLogDebug("Fragment Shader Created");
   
   // Load Fragment Shader
   length = strlen(FragmentShaderText);
   glShaderSource(fragmentShader, 1, &FragmentShaderText, &length);
   error = glGetError();
   rlmAssert(error == GL_NO_ERROR, "Error loading Fragment Shader Source: 0x%04X", error);
   rlmLogDebug("Fragment Shader Loaded");
   
   // Compile Fragment Shader
   glCompileShader(fragmentShader);
   glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
   if(success == GL_FALSE)
   {
      GLint maxLength = 0;
      char * log;
      glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);
      log = malloc(maxLength);
      glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, log);
      rlmLogError("Fragment Shader Error Log:\n%s", log);
      free(log);
      glDeleteShader(fragmentShader);
      glDeleteShader(vertexShader);
      rlmLogFatal("Fragment Shader Failed to Build");
   }
   rlmLogDebug("Fragment Shader Compiled");

   // Create Program
   program = glCreateProgram();
   rlmAssert(program != 0, "Failed to Create Shader Program: 0x%04X", glGetError());
   rlmLogDebug("Shader Program Created");
   
   // Attach Shaders to program
   glAttachShader(program, vertexShader);
   glAttachShader(program, fragmentShader);
   
   glLinkProgram(program);
   glGetProgramiv(program, GL_LINK_STATUS, &success);
   if(success == GL_FALSE)
   {
      GLint maxLength = 0;
      char * log;
      glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
      log = malloc(maxLength);
      glGetProgramInfoLog(program, maxLength, &maxLength, log);
      rlmLogError("Shader Program Link Error Log:\n%s", log);
      free(log);
      glDetachShader(program, vertexShader);
      glDetachShader(program, fragmentShader);
      glDeleteProgram(program);
      glDeleteShader(vertexShader);
      glDeleteShader(fragmentShader);
      rlmLogFatal("Shader Program Failed to Link");
   }
   rlmLogDebug("Shader Program Linked");

   glDetachShader(program, vertexShader);
   glDetachShader(program, fragmentShader);
   glDeleteShader(vertexShader);
   glDeleteShader(fragmentShader);
   return program;
}

static inline 
struct ricgOpenGLRenderData * rcigbopenglCreateData(void)
{
   struct ricgOpenGLRenderData * data = malloc(sizeof(struct ricgOpenGLRenderData));
   
   data->shader = rcigbopenglCreateShaderProgram();
   
   // Get Uniform locations
   data->uniformMatrix = glGetUniformLocation(data->shader, "matrix");
   rlmAssert(data->uniformMatrix != -1, "Failed to find uniform address for \"matrix\"");
   
   data->uniformTexture = glGetUniformLocation(data->shader, "theTexture");
   rlmAssert(data->uniformTexture != -1, "Failed to find uniform address for \"theTexture\"");
   
   
   data->whiteTexture = rcigbopenglCreateWhiteTexture(data);
   
   return data;
}

void rcigbopenglSetupRender(struct rcigRenderingBackend * render)
{
   render->name = "OpenGL";
   render->data = rcigbopenglCreateData();
   render->fpTeardown = rcigbopenglTeardown;
   render->fpCreateTextureFromU8RGBAData = rcigbopenglCreateTextureFromU8RGBAData;
   render->fpFreeTexture = rcigbopenglFreeTexture;
   render->fpDraw = rcigbopenglDraw;
}
