#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>

#include "RyanLogger.h"

static
enum ryan_logger_level rlMinimumLevel = rlLogLevelDebug;

void rlSetMinimumLevel(enum ryan_logger_level level)
{
   rlMinimumLevel = level;
}

static inline
const char * rlShortenFilename(const char * filename)
{
   const char * output = filename;
   int i = 0;
   while(filename[i] != '\0')
   {
      if(filename[i] == '\\' || filename[i] == '/')
      {
         output = filename + i + 1;
      }
      i++;
   }
   return output;
}

const char * rlGetLevelText(enum ryan_logger_level level)
{
   const char * text;
   switch(level)
   {
   case rlLogLevelNone:
      text = "none";
      break;
   case rlLogLevelTrace: 
      text = "trace";
      break;
   case rlLogLevelDebug: 
      text = "debug";
      break;
   case rlLogLevelInfo:
      text = "info";
      break;
   case rlLogLevelWarning:
      text = "warning";
      break;
   case rlLogLevelError:
      text = "error";
      break;
   case rlLogLevelFatal:
   default:
      text = "fatal";
      break;
   }
   return text;
}

void rlLog(const char * filename, int line, enum ryan_logger_level level, const char * format, ...)
{
   va_list argp;
   const char * shortFilename;
   const char * levelText;
   
   if(level >= rlMinimumLevel)
   {
      shortFilename = rlShortenFilename(filename);
      levelText = rlGetLevelText(level);
      va_start(argp, format);
      printf("%s:%d: %s: ", shortFilename, line, levelText);
      vprintf(format, argp);
      printf("\n");
      va_end(argp);
      if(level == rlLogLevelFatal)
      {
         exit(1);
      }
   }
}
