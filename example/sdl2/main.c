#include <stdio.h>
#include <stdbool.h>

#include <SDL.h>
#include <GL/glew.h>
#include <SDL_opengl.h>
#include <GL/gl.h>
//#include <SDL_image.h>

#include <RyanLogger.h>
#include <RyanCIG.h>
#include <RCIGBackendSDL2.h>
#include <RCIGBackendOpenGL.h>

#define SCREEN_WIDTH  1024
#define SCREEN_HEIGHT 768


/*
static inline
SDL_Texture * SDLLoadImage(SDL_Renderer * rend, const char * imageFilename)
{
   SDL_Surface * surf;
   SDL_Texture * text;
   surf =  IMG_Load(imageFilename);
   rlmAssert(surf != NULL, "Failed to load Image \"%s\" becuase: %s", imageFilename, IMG_GetError());
   text = SDL_CreateTextureFromSurface(rend, surf);
   rlmAssert(text != NULL, "Failed to convert surface to texture for image \"%s\" Reason: %s", imageFilename, SDL_GetError());

   SDL_FreeSurface(surf);
   rlmLogInfo("Loaded Image: %s", imageFilename);
   return text;
}
*/


static inline
void LimmitFrames(Uint32 * milliseconds, Uint32 limmitTo_ms)
{
   Uint32 now_ms = SDL_GetTicks();
   Uint32 diff = now_ms - *milliseconds;
   (*milliseconds) = now_ms;
   if(diff < limmitTo_ms)
   {
      SDL_Delay(limmitTo_ms - diff);
   }
}

static inline
float GetDeltaTime_seconds(Uint32 * milliseconds)
{
   Uint32 now_ms = SDL_GetTicks();
   Uint32 diff = now_ms - *milliseconds;
   (*milliseconds) = now_ms;
   return diff / 1000.0f;
}

int main(int argc, char * args[])
{
   SDL_Window * window;
   SDL_GLContext * glContext;
   SDL_Event event;
   bool running;
   Uint32 limmit_ms;
   Uint32 timer_ms;
   GLenum glewResult;
   
   struct rcigBackend backend;
   
   //rlSetMinimumLevel(rlLogLevelTrace);
  
   rlmAssert(SDL_Init(SDL_INIT_EVERYTHING) == 0, "Couldn't Initialize SDL: %s", SDL_GetError());
   rlmLogInfo("SDL Initialized");
   //rlmAssert((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) == IMG_INIT_PNG, "Couldn't Initialize SDL_Image: %s", IMG_GetError());
   //rlmLogInfo("SDL_Image Initialized");
   
   window = SDL_CreateWindow("SDL2 Example", SDL_WINDOWPOS_UNDEFINED,
                                             SDL_WINDOWPOS_UNDEFINED,
                                             SCREEN_WIDTH, SCREEN_HEIGHT, 
                                             SDL_WINDOW_OPENGL);
   rlmAssert(window != NULL, "SDL Failed to open window: %s", SDL_GetError());
   rlmLogInfo("SDL Window Created");
   
   glContext = SDL_GL_CreateContext(window);
   rlmAssert(glContext != NULL, "SDL Failed to get GL Context: %s", SDL_GetError());
   rlmLogInfo("GL Context Created");
   
   glewResult = glewInit();
   rlmAssert(glewResult == GLEW_OK, "GLEW failed to initialize: %s", glewGetErrorString(glewResult));
   rlmLogInfo("GLEW Initialized Created");

   rcigbsdl2SetupInput(&backend.input);
   rcigbopenglSetupRender(&backend.render);
   
   
   rcigSetup(&backend, SCREEN_WIDTH, SCREEN_HEIGHT);
   {
      float horiDPI, vertDPI;
      int displayIndex = SDL_GetWindowDisplayIndex(window);
      SDL_GetDisplayDPI(displayIndex, &horiDPI, &vertDPI, NULL);
      rcigSetScreenDPI(horiDPI, vertDPI);
   }
   rcigLoadFont("ProggyClean.ttf", 13);
   //rcidCreateFontTexture();
   rlmLogInfo("Ryan CIG Setup");

   
   rlmLogInfo("Starting Main Loop");
   running = true;
   timer_ms = limmit_ms = SDL_GetTicks();
   while(running)
   {
      float dt_seconds;
      
      while(SDL_PollEvent(&event))
      {
         rcigbsdl2HandleEvent(&backend.input, &event);
         switch(event.type)
         {
         case SDL_QUIT:
            rlmLogInfo("SDL Event: Quit");
            running = false;
            break;
         case SDL_KEYDOWN:
            switch(event.key.keysym.sym)
            {
            case SDLK_ESCAPE:
               rlmLogInfo("Escape key pressed");
               running = false;
               break;
            }
            break;
         case SDL_KEYUP:
            break;
         case SDL_MOUSEBUTTONDOWN:
            break;
         case SDL_MOUSEBUTTONUP:
            break;
         case SDL_MOUSEWHEEL:
            break;
         case SDL_MOUSEMOTION:
            break;
         }
      }

      // Get Delta Time
      dt_seconds = GetDeltaTime_seconds(&timer_ms);
      
      
      rcigPanelBegin();
         rcigLabel("Hello World!");
         //rcigLabel("Hello World!");
         rcigLabel("g is a text");
         rcigLabel("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!:#$\"0123456789");
      rcigPanelEnd();
      
      glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
      glClearColor(0.0f, 0.0f, 0.2f, 0.f);
      //glClearColor(1.0f, 1.0f, 1.0f, 0.f);
      glClear(GL_COLOR_BUFFER_BIT);      
      
      // Draw Stuff Here
      rcigRender();
      
      SDL_GL_SwapWindow(window);

      LimmitFrames(&limmit_ms, 17); // limmit to 60 fps
   }
   rlmLogInfo("Exiting Main Loop");
   
   
   rcigTeardown();
   
   SDL_DestroyWindow(window);
   rlmLogInfo("SDL Window Destroyed");
   //IMG_Quit();
   //rlmLogInfo("SDL_Image Quit");
   SDL_Quit();
   rlmLogInfo("SDL Quit");
   
   rlmLogInfo("End of Program");
   return 0;
}
