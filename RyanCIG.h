#ifndef __RYANCIG_H__
#define __RYANCIG_H__

struct rcigInputBackend
{
   const char * name;
   void * data;
   void (*fpTeardown)(void * data);
};

enum rcigDrawElementType
{
   eRCIGDET_Points,
   eRCIGDET_Lines,
   eRCIGDET_Triangles
};

struct rcigGeometryData
{
   float x;
   float y;
   float u;
   float v;
   float colorR;
   float colorG;
   float colorB;
   float colorA;
};

struct rcigDrawElement
{
   enum rcigDrawElementType type;
   unsigned int indexDataCount;
   const unsigned short * indexData;
   unsigned int geometryDataCount;
   const struct rcigGeometryData * geometryData;
   const void * texture;
};

enum rcigImageFilterHint
{
   eRCIGIFH_Nearest, // Think pixel art
   eRCIGIFH_Linear   // Some smoothing
};

struct rcigRenderingBackend
{
   const char * name;
   void * data;
   void (*fpTeardown)(void * data);
   void * (*fpCreateTextureFromU8RGBAData)(void * data, const void * imageData, 
                                           unsigned int width, unsigned int height,
                                           enum rcigImageFilterHint filterHint);
   void (*fpFreeTexture)(void * data, void * texture);
   void (*fpDraw)(void * data, unsigned int drawElementCount, 
                               const struct rcigDrawElement * drawElementList,
                               int windowWidth, int windowHeight);
};

struct rcigBackend
{
   struct rcigInputBackend     input;
   struct rcigRenderingBackend render;
};

struct rcigFont;

void rcigSetup(const struct rcigBackend * backend, int windowWidth, int windowHeight);
void rcigTeardown(void);

void rcigRender(void);


void rcigSetScreenDPI(float horiDPI, float vertDPI);

struct rcigFont * rcigLoadFont(const char * filename, float pointSize);
void rcidCreateFontTexture(void);

void rcigFontPush(const struct rcigFont * font);
struct rcigFont * rcigFontPop(void);


struct rcigComponent
{
   void * (*fpCreate)(const void * componentSpecData);
   void (*fpDestroy)(void * componentData);
   int (*fpCompair)(const void * componentData, const void * componentSpecData);
   void (*fpRender)(void * componentData, int x, int y, int * width, int * height);
};


void rcigPanelBegin(void);
void rcigPanelEnd(void);

// Built In Components
void rcigLabel(const char * text);

// Custom Component API
void * rcigComponent(const struct rcigComponent * component, const void * componentSpecData);

void rcigQueueDrawElement(const struct rcigDrawElement * element);
void rcigDrawElementSetTexturedQuad(struct rcigDrawElement * element,
                                    struct rcigGeometryData * geometryData,
                                    void * texture, int x, int y, 
                                    int width, int height);

void rcigFreeTexture(void * texture);

static inline 
void rcigGeometryDataSet(struct rcigGeometryData * data, float x, float y,
                                                         float u, float v,
                                                         float colorR, 
                                                         float colorG, 
                                                         float colorB, 
                                                         float colorA)
{
   data->x = x;
   data->y = y;
   data->u = u;
   data->v = v;
   data->colorR = colorR;
   data->colorG = colorG;
   data->colorB = colorB;
   data->colorA = colorA;
}




#endif // __RYANCIG_H__
